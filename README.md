* **v0.10**
  * Identified the last secret area, I never encountered Evil Lu before today

* **v0.9**
  * Moved secret passages to a new layer, added disorder links

* **v0.8**
  * Finished splitting up the secret areas (except for three screens with no outgoing links)

* **v0.7**
  * Used AA's KUEditor, added a whole bunch of cool stuff. Being able to see the room scripts is neat

* **v0.6**
  * Added some links from the Disorder to the overworld to aid fast travel

* **v0.5**
  * Added interlude
  * Added chapters 1 and 2 shading

* **v0.4**
  * Redid existing areas based on grid

* **v0.3**
  * Added grid
  * Added link from mathias teleporter to west room

* **v0.2**
  * Added link from menu north area to secret dance puzzle

* **v0.1**
  * Initial
